package main;

import org.w3c.dom.Text;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Calculator {


    public static void main(String[] args) {



        CalcFrame calcFrame = new CalcFrame();
        calcFrame.addEventPanel();
        calcFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        calcFrame.setVisible(true);
        calcFrame.setResizable(false);

        calcFrame.setSize(185,290);

    }

}

class CalcFrame extends JFrame {

    String Text2 = "+";
    int NUM = 12;


    JPanel mMainPanel = new JPanel();
    NumberPanel mNumberPanel = new NumberPanel();
    ButtonPanel mButtonPanel = new ButtonPanel();
    TextPanel mTextPanel = new TextPanel();

    public CalcFrame() {
        mMainPanel.setLayout(new BorderLayout());
        mMainPanel.add(mTextPanel, BorderLayout.NORTH);
        mMainPanel.add(mNumberPanel, BorderLayout.WEST);
        mMainPanel.add(mButtonPanel, BorderLayout.EAST);

        this.add(mMainPanel);
    }
    public void addEventPanel() {
        JButton[] mNumberButtons = mNumberPanel.mNumberButtons;
        JButton[] maritextButtoms = mButtonPanel.arithmetics;
        mNumberPanel.mNumberButtons[1].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "1");

            }
        });
        mNumberPanel.mNumberButtons[2].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "2");
            }
        });
        mNumberPanel.mNumberButtons[3].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                mTextPanel.Text.setText(mTextPanel.Text.getText() + "3");

            }
        });
        mNumberPanel.mNumberButtons[4].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "4");
            }
        });
        mNumberPanel.mNumberButtons[5].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "5");
            }
        });
        mNumberPanel.mNumberButtons[6].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "6");
            }
        });
        mNumberPanel.mNumberButtons[7].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "7");
            }
        });
        mNumberPanel.mNumberButtons[8].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "8");
            }
        });
        mNumberPanel.mNumberButtons[9].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "9");
            }
        });
        mNumberPanel.commaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText(mTextPanel.Text.getText() + ".");
            }
        });
        mNumberPanel.mZeroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "0");
            }
        });
        mNumberPanel.mZeroChangeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mTextPanel.Text.setText("");
            }
        });
        mButtonPanel.arithmetics[0].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mNumberPanel.tempNumberone = Integer.parseInt(mTextPanel.Text.getText());
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "+");
                mTextPanel.mArthe = "\\+";//
            }
        });
        mButtonPanel.arithmetics[1].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mNumberPanel.tempNumberone = Integer.parseInt(mTextPanel.Text.getText());
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "-");
                  mTextPanel.mArthe = "-";//

            }
        });
        mButtonPanel.arithmetics[2].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mNumberPanel.tempNumberone = Integer.parseInt(mTextPanel.Text.getText());
                 mTextPanel.Text.setText(mTextPanel.Text.getText() + "*");
                    mTextPanel.mArthe = "\\*";

            }
        });
        mButtonPanel.arithmetics[3].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mNumberPanel.tempNumberone = Integer.parseInt(mTextPanel.Text.getText());
                mTextPanel.Text.setText(mTextPanel.Text.getText() + "/");
                 mTextPanel.mArthe ="/";

            }
        });
       mButtonPanel.arithmetics[4].addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               String textFieldText = mTextPanel.Text.getText();
               String arthemics = mTextPanel.mArthe;


                   int firstNumber = Integer.parseInt(textFieldText.split(arthemics)[0]);
                   int secondNumber = Integer.parseInt(textFieldText.split(arthemics)[1]);
                   switch (mTextPanel.mArthe) {
                       case "\\+" : int result = firstNumber + secondNumber;
                           mTextPanel.Text.setText(String.valueOf(result));
                           break;
                        case "-" : int tworesult = firstNumber - secondNumber;
                            mTextPanel.Text.setText(String.valueOf(tworesult));
                           break;
                       case "\\*" : int thirdresult = firstNumber * secondNumber;
                           mTextPanel.Text.setText(String.valueOf(thirdresult));
                           break;
                       case "/" : float fourresult = (float) firstNumber / secondNumber;
                           mTextPanel.Text.setText(String.valueOf(fourresult));
                           break;
                   }
           }
       });
    }

}

class TextPanel extends JPanel {
    JTextField Text = new JTextField(20);
    String mArthe = "";


    public TextPanel() {
        setLayout(new GridLayout(1,1));
        Text.setHorizontalAlignment(SwingConstants.RIGHT);
        this.add(Text);
    }
}

/**
 *
 */
class NumberPanel extends JPanel {
     int tempNumberone = 0;

    JButton[] mNumberButtons = new JButton[10];
    JButton mZeroChangeButton = new JButton("C");
    JButton commaButton = new JButton(".");
    JButton mZeroButton  = new JButton("0");

    /**
     *
     */
    public NumberPanel() {
        this.setLayout(new GridLayout(5,3));
        this.add(mZeroChangeButton);
        for(int calculatorNumber = 1; calculatorNumber < mNumberButtons.length; calculatorNumber++) {
            mNumberButtons[calculatorNumber] = new JButton(String.valueOf(calculatorNumber));

            this.add(mNumberButtons[calculatorNumber]);

        }
        this.add(commaButton);
        this.add(mZeroButton);
    }
}

/**
 *
 */
class ButtonPanel extends JPanel {
    JButton[] arithmetics = new JButton[5];
    public ButtonPanel() {
        this.setLayout(new GridLayout(5,1));
        arithmetics[0] = new JButton("+");
        arithmetics[1] = new JButton("-");
        arithmetics[2] = new JButton("X");
        arithmetics[3] = new JButton("/");
        arithmetics[4] = new JButton("=");
        this.add(arithmetics[0]);
        this.add(arithmetics[1]);
        this.add(arithmetics[2]);
        this.add(arithmetics[3]);
        this.add(arithmetics[4]);
    }
}